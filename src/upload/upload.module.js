(function () {

  'use strict';
  
  require('angular-route');
  require('ng-file-upload');
  require('../app.config.js');
  require('../documents/documents.module');

  angular
    .module('app.upload', [
      'ngRoute',
      'ngFileUpload',
      'app.config',
      'app.documents'
    ]);

  require('./upload.config');
  require('../documents/documents.service');
  require('../documents/documents.directive');

})();
