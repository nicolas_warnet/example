(function () {

  'use strict';

  var gulp = require('gulp'),
    ngConfig = require('gulp-ng-config'),
    merge = require('gulp-merge-json');

  /**
   * Generate app.config.js (create constants according to app.config.json).
   * This task first executes 'combine'.
   */
  gulp.task('default', ['combine'], function() {
   return gulp.src('./dist/app.config.json')
      .pipe(ngConfig('app.config', {
        pretty: true
      }))
      .pipe(gulp.dest('./src'));
  });

  /**
   * Merge default.config.json and local.config.json files into app.config.json.
   */
  gulp.task('combine', function () {
    return gulp.src(['./src/**/*.json', './local.config.json'])
      .pipe(merge('app.config.json'))
      .pipe(gulp.dest('./dist'));
  });
  
})();
