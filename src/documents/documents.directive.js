(function () {

  'use strict';

  angular
    .module('app.documents')
    .directive('documents', Documents);

  function Documents() {
    return {
      restrict: 'A',
      templateUrl: 'src/documents/documents.view.html',
      link: function (scope, element, attrs) {
        scope[ attrs.documents ];
      }
    };
  }

})();
