(function () {

  'use strict';

  require('../app.config.js');

  angular
    .module('app.documents', []);

})();
