(function () {

    'use strict';

    angular
      .module('app.upload')
      .controller('UploadController', UploadController);

    UploadController.$inject = ['$scope', 'Upload', 'ENV_VARS', 'DocumentsService'];

    function UploadController ($scope, Upload, ENV_VARS, DocumentsService) {

        var vm = this;

        vm.form = {};
        vm.m;
        vm.analysed = false;
        vm.result = {};

        DocumentsService.getDocuments().then(function (result) {
            $scope.documents = result;
        });

        vm.addMarker = function() {
            if(!vm.form.markers)
                vm.form.markers = [];

            vm.form.markers.push({
                text: vm.m,
                after: 'before'
            });
            vm.m = undefined;
        };

        vm.removeMarker = function(index) {
            vm.form.markers.splice(index,1);
        };

        vm.cancelDocument = function() {
            delete vm.form.document;
        };

        vm.uploadFiles = function() {
            if (!vm.form.$invalid)
                Upload.upload({
                    url: ENV_VARS.uploadUrl,
                    method: 'POST',
                    data: vm.form
                }).then(function (response) {
                    vm.analysed = true;
                    vm.form = {};
                    vm.progress = 0;
                    vm.result = response.data;
                }, function (response) {
                    vm.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    vm.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
        }
    }

})();
