(function () {

  'use strict';

  angular
    .module('app.documents')
    .service('DocumentsService', DocumentsService);

  DocumentsService.$inject = ['$http', '$timeout', '$q', 'ENV_VARS'];

  function DocumentsService($http, $timeout, $q, ENV_VARS) {
    return {
      getDocuments: function () {
        var defer = $q.defer();
        $timeout(function () {
          $http.get(ENV_VARS.apiUrl).success(function (result) {
            defer.resolve(result);
          });
        }, 2000);
        return defer.promise;
      }
    };
  }

})();
