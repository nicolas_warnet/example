(function () {

  'use strict';
  
  var angular = require('angular');

  require('./upload/upload.module');

  angular
    .module('app', [
      'app.upload'
    ]);

})();
